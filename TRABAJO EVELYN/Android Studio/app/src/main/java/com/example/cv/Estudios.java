package com.example.cv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class Estudios extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudios);


    }

    public void onClick(View view) {
        Intent miIntent = null;
        switch (view.getId()){
            case R.id.button5:
                 miIntent = new Intent(Estudios.this,MainActivity.class);
                break;

            case R.id.button3:
                miIntent = new Intent(Estudios.this,HabilidadesConocimientos.class);
                break;



        }
        startActivity(miIntent);
    }
}