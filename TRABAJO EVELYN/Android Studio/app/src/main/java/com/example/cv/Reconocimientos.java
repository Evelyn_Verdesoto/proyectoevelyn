package com.example.cv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Reconocimientos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconocimientos);
    }

    public void onClick(View view) {
        Intent miIntent = null;
        switch (view.getId()){
            case R.id.button7:
                miIntent = new Intent (Reconocimientos.this,HabilidadesConocimientos.class);
                break;

            case R.id.button6:
                miIntent = new Intent(Reconocimientos.this,ReferenciasPersonales.class);
                break;



        }
        startActivity(miIntent);
    }

}