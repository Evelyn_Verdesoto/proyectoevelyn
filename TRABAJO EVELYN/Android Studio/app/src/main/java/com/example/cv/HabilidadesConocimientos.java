package com.example.cv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HabilidadesConocimientos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habilidades_conocimientos);
    }

    public void onClick(View view) {
        Intent miIntent = null;
        switch (view.getId()){
            case R.id.button4:
                miIntent = new Intent(HabilidadesConocimientos.this,Estudios.class);
                break;

            case R.id.button2:
                miIntent = new Intent(HabilidadesConocimientos.this,Reconocimientos.class);
                break;



        }
        startActivity(miIntent);
    }

}